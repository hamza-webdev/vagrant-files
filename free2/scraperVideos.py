#!/usr/bin/python3

from selenium import webdriver
import time,datetime,json,signal,sys,click,os,yaml
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
import pika
import libs.elastic_querier as eq



yaml_settings = dict()

class GracefulKiller:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self,signum, frame):
    self.kill_now = True

class Link():

  def __init__(self, title, href, created_at, channel):
    self.title = title
    self.href = href.replace("https://www.youtube.com/watch?v=","")
    self.created_at = created_at
    self.channel = channel

  def getHref(self):
    return self.href

  def getTitle(self):
    return self.title

  def getCreatedAt(self):
    return self.created_at

  def getChannel(self):
    return self.channel

  def jsonMessage(self):
    message = { "createdAt": self.created_at,"href": self.href, "title": self.title, "channel": self.channel }
    message = json.dumps(message)
    return message

  def pushToQueue(self,rmq_host,rmq_queue):
    connection = pika.BlockingConnection(pika.ConnectionParameters(rmq_host))
    channel = connection.channel()
    channel.queue_declare(queue=rmq_queue,durable=True)
    channel.basic_publish(exchange='',
                 routing_key=rmq_queue,
                 properties=pika.BasicProperties(
                   delivery_mode = 2
                 ),
                 body=self.jsonMessage())
    print(self.jsonMessage())

def scraper(scheme,host,port,index,rmq_host,rmq_queue):
  options = Options()
  options.headless = True
  options.add_experimental_option("excludeSwitches", ['enable-automation'])
  options.add_argument('--no-sandbox')
  options.add_argument('--disable-dev-shm-usage')
  options.add_argument('--disable-gpu')

  browser = webdriver.Chrome(options=options)
  killer = GracefulKiller()

  while True:
    es_api = eq.get_elasticsearch_connect(scheme,host,port)
    channel = eq.waitingChannels(es_api,index)[0][0]
    eq.setStatusChannel(es_api,index,channel,'processing')
    browser.get('https://www.youtube.com/c/' + channel  + '/videos')
    time.sleep(3)

    try:
      browser.find_element(By.XPATH,"//*[contains(text(), 'I agree')]").click()
    except:
      pass

      time.sleep(3)

    try: 
      button = browser.find_element(By.XPATH, '/html/body/div/c-wiz/div/div/div/div[2]/div[1]/div[4]/form/div[1]').click()
    except:
      pass

    time.sleep(1)
    ht = ""

    try:
      elem = browser.find_element(By.TAG_NAME,"body")
      ht = browser.execute_script("return document.documentElement.scrollHeight;")
    except:
      pass
    
    while True:

      prev_ht = ""

      try:
        prev_ht = browser.execute_script("return document.documentElement.scrollHeight;")
        browser.execute_script("window.scrollTo(0, document.documentElement.scrollHeight);")
        time.sleep(2)
        ht = browser.execute_script("return document.documentElement.scrollHeight;")
      except:
        pass
      if prev_ht == ht:
        break
      if killer.kill_now:
        browser.close()
        es_api = eq.get_elasticsearch_connect(es_api,scheme,host,port)
        eq.setStatusChannel(es_api,index,channel,'waiting')
        sys.exit(0)
        break
    if killer.kill_now:
      try:
        browser.close()
      except:
        pass
      es_api = eq.get_elasticsearch_connect(scheme,host,port)
      eq.setStatusChannel(es_api,index,channel,'waiting')
      sys.exit(0)
      break

    links=browser.find_elements(By.XPATH, '//*[@id="video-title"]')
    count_videos = 0
    list_videos = []
    list_links = []
    
    for link in links:
        count_videos += 1
        title = link.get_attribute("title")
        href = link.get_attribute("href")
        list_videos.append(link.get_attribute("href"))
        video = Link(title,href,datetime.datetime.now().isoformat(timespec='microseconds'),channel)
        list_links.append(video)
        video.pushToQueue(rmq_host,rmq_queue)
        if killer.kill_now:
          try:
            browser.close()
          except:
            pass
          es_api = eq.get_elasticsearch_connect(scheme,host,port)
          eq.setStatusChannel(es_api,index,channel,'waiting')
          sys.exit(0)
          break

    eq.setStatusChannel(es_api,index,channel,'waiting')

  browser.close()

@click.command()
@click.option('--run', '-r', is_flag=True, help="run scraper")
@click.option('--config', '-C', help="config location")
def cli(run,config):
  if config:

    if os.path.isfile(config):
    
      with open(config, "r") as ymlfile:
        yaml_settings = yaml.load(ymlfile, Loader=yaml.FullLoader)
    
      scheme = yaml_settings['elasticsearch']['scheme']
      host   = yaml_settings['elasticsearch']['host']
      port   = yaml_settings['elasticsearch']['port']
      index  = yaml_settings['elasticsearch']['index']

      rmq_host    = yaml_settings['rabbitmq']['host']
      rmq_queue   = yaml_settings['rabbitmq']['queue']
    
    else:
      print("Erreur - no config.yaml file or environement variables")
      sys.exit(1)
  
  if run:
    scraper(scheme,host,port,index,rmq_host,rmq_queue)    

if __name__ == '__main__':
  try:
    cli()
    sys.exit(0)
  except:
    sys.exit(1)
