#!/usr/bin/python3

import datetime,logging
import elasticsearch as elastic

def get_elasticsearch_connect(scheme,host,port):
  """Get a connection for Elasticsearch"""
  connect = "{}://{}:{}".format(scheme, host, port)
  es_logger = logging.getLogger('elasticsearch')
  es_logger.setLevel(logging.WARNING)
  return elastic.Elasticsearch(connect)

def queryElastic(es_api,index,request=None,fields=None):
  """Request Elasticsearch with specific request or filter or not"""
  if request is None:
    request = { "match_all" : {}}
  if fields is None:
    fields = ""
  get_docs = es_api.search(index = index, query = request, filter_path = fields)
  return get_docs

def waitingChannels(es_api,index):
  request = { "match" : {'status': 'waiting'}}
  fields = ['hits.hits._source.name', 'hits.hits._source.status', 'hits.hits._source.createdAt']
  waiting_channels = queryElastic(es_api,index,request,fields)
  lst_waiting = []
  for hit in waiting_channels['hits']['hits']:
    doc = hit['_source']
    lst_waiting.append([doc['name'],doc['status'],doc['createdAt']])
  lst_waiting = sorted(lst_waiting, key=lambda t: datetime.datetime.strptime(t[2],'%Y-%m-%dT%H:%M:%S.%f'))
  return lst_waiting

def waitingVideos(es_api,index):
  request = { "match" : {'status': 'waiting'}}
  fields = ""
  waiting_videos = queryElastic(es_api,index,request,fields)
  lst_waiting = []
  for hit in waiting_videos['hits']['hits']:
    doc = hit['_source']
    lst_waiting.append([doc['href'],doc['status'],doc['createdAt']])
  lst_waiting = sorted(lst_waiting, key=lambda t: datetime.datetime.strptime(t[2],'%Y-%m-%dT%H:%M:%S.%f'))
  return lst_waiting

def updateDoc(es_api,index,doc,field,value):
  update_doc = es_api.update(index = 'videos', id = doc, body = {'doc': { field: value, 'createdAt':datetime.datetime.now().isoformat(timespec='microseconds')}})
  return update_doc

def setStatusChannel(es_api,index,channel,status):
  body = {'createdAt':datetime.datetime.now().isoformat(timespec='microseconds'),'name': channel, 'status': status }
  register_channel = es_api.index(index = index, id = channel, document = body)
  return register_channel

def insertChannel(es_api,index,channel):
  body = {'createdAt':datetime.datetime.now().isoformat(timespec='microseconds'),'name': channel, 'status': 'waiting'}
  register_channel = es_api.index(index = index, id = channel, document = body)
  return register_channel

def removeChannel(es_api,index,channel):
  remove_result = es_api.delete(index = index, id = channel, ignore = [400, 404])
  return remove_result

