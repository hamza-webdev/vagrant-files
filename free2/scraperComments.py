#!/usr/bin/python3

from selenium import webdriver
import time,datetime,json,signal,sys,click,os,yaml
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
import pika
import libs.elastic_querier as eq



yaml_settings = dict()

class GracefulKiller:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self,signum, frame):
    self.kill_now = True

class Link():

  def __init__(self, title, href, created_at, channel):
    self.title = title
    self.href = href.replace("https://www.youtube.com/watch?v=","")
    self.created_at = created_at
    self.channel = channel

  def getHref(self):
    return self.href

  def getTitle(self):
    return self.title

  def getCreatedAt(self):
    return self.created_at

  def getChannel(self):
    return self.channel

  def jsonMessage(self):
    message = { "createdAt": self.created_at,"href": self.href, "title": self.title, "channel": self.channel }
    message = json.dumps(message)
    return message

  def pushToQueue(self,rmq_host,rmq_queue):
    connection = pika.BlockingConnection(pika.ConnectionParameters(rmq_host))
    channel = connection.channel()
    channel.queue_declare(queue=rmq_queue,durable=True)
    channel.basic_publish(exchange='',
                 routing_key=rmq_queue,
                 properties=pika.BasicProperties(
                   delivery_mode = 2
                 ),
                 body=self.jsonMessage())
    print(self.jsonMessage())

def scraper(scheme,host,port,index,rmq_host,rmq_queue):
  options = Options()
  options.headless = True
  options.add_experimental_option("excludeSwitches", ['enable-automation'])
  options.add_argument('--no-sandbox')
  options.add_argument('--disable-dev-shm-usage')
  options.add_argument('--disable-gpu')

  browser = webdriver.Chrome(options=options)
  killer = GracefulKiller()

  while True:

    es_api = eq.get_elasticsearch_connect(scheme,host,port)
    video = eq.waitingVideos(es_api,index)[0][0]
    print(video)
    eq.updateDoc(es_api,index,video,'status','processing')
    time.sleep(5)
    eq.updateDoc(es_api,index,video,'status','done')
    print('end')
  browser.close()

@click.command()
@click.option('--run', '-r', is_flag=True, help="run scraper")
@click.option('--config', '-C', help="config location")
def cli(run,config):
  if config:

    if os.path.isfile(config):
    
      with open(config, "r") as ymlfile:
        yaml_settings = yaml.load(ymlfile, Loader=yaml.FullLoader)
    
      scheme = yaml_settings['elasticsearch']['scheme']
      host   = yaml_settings['elasticsearch']['host']
      port   = yaml_settings['elasticsearch']['port']
      index  = yaml_settings['elasticsearch']['index']

      rmq_host    = yaml_settings['rabbitmq']['host']
      rmq_queue   = yaml_settings['rabbitmq']['queue']
    
    else:
      print("Erreur - no config.yaml file or environement variables")
      sys.exit(1)
  
  if run:
    scraper(scheme,host,port,index,rmq_host,rmq_queue)    

if __name__ == '__main__':
  try:
    cli()
    sys.exit(0)
  except:
    sys.exit(1)
